/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "campus")
public class Campus implements Serializable {

    @Id
    private String campusName;
    private String campusAddress;
    private String campusDistance;
    private String busNo;

    public Campus() {
    }

    public Campus(String campusName, String campusAddress, String campusDistance, String busNo) {
        this.campusName = campusName;
        this.campusAddress = campusAddress;
        this.campusDistance = campusDistance;
        this.busNo = busNo;
    }

    public String getBusNo() {
        return busNo;
    }

    public void setBusNo(String busNo) {
        this.busNo = busNo;
    }

    public String getCampusName() {
        return campusName;
    }

    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

    public String getCampusAddress() {
        return campusAddress;
    }

    public void setCampusAddress(String campusAddress) {
        this.campusAddress = campusAddress;
    }

    public String getCampusDistance() {
        return campusDistance;
    }

    public void setCampusDistance(String campusDistance) {
        this.campusDistance = campusDistance;
    }

}
