/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "faculty")
public class Faculty implements Serializable {

    @Id
    private String facultyName;
    private String deanName;
    private String facultyBuilding;

    public Faculty() {
    }

    public Faculty(String facultyName, String deanName, String facultyBuilding) {
        this.facultyName = facultyName;
        this.deanName = deanName;
        this.facultyBuilding = facultyBuilding;
    }

    public String getFacultyBuilding() {
        return facultyBuilding;
    }

    public void setFacultyBuilding(String facultyBuilding) {
        this.facultyBuilding = facultyBuilding;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getDeanName() {
        return deanName;
    }

    public void setDeanName(String deanName) {
        this.deanName = deanName;
    }

}
