/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "precourse")
public class PreCourse implements Serializable {

    @Id
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Course course;
    @Id
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Course preCourse;

    public PreCourse() {
    }

    public PreCourse(Course course, Course preCourse) {
        this.course = course;
        this.preCourse = preCourse;
    }

    public Course getPreCourse() {
        return preCourse;
    }

    public void setPreCourse(Course preCourse) {
        this.preCourse = preCourse;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

}
