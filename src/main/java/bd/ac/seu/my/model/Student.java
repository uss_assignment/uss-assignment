/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "student")
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int studentId;
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Programme programme;
    private String studentName;
    private String studentBirthDay;
    private String yearEnroll;

    public Student() {
    }

    public Student(int studentId, Programme programme, String studentName, String studentBirthDay, String yearEnroll) {
        this.studentId = studentId;
        this.programme = programme;
        this.studentName = studentName;
        this.studentBirthDay = studentBirthDay;
        this.yearEnroll = yearEnroll;
    }
    
    
    public String getYearEnroll() {
        return yearEnroll;
    }

    public void setYearEnroll(String yearEnroll) {
        this.yearEnroll = yearEnroll;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public Programme getProgramme() {
        return programme;
    }

    public void setProgramme(Programme programme) {
        this.programme = programme;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentBirthDay() {
        return studentBirthDay;
    }

    public void setStudentBirthDay(String studentBirthDay) {
        this.studentBirthDay = studentBirthDay;
    }

}
