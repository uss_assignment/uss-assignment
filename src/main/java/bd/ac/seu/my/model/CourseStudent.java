/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "course_student")
public class CourseStudent implements Serializable {

    @Id
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Course course;
    @Id
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Student student;
    private String yearTaken;
    private String semesterTaken;
    private String gradeAwarded;

    public CourseStudent() {
    }

    public CourseStudent(Course course, Student student, String yearTaken, String semesterTaken, String gradeAwarded) {
        this.course = course;
        this.student = student;
        this.yearTaken = yearTaken;
        this.semesterTaken = semesterTaken;
        this.gradeAwarded = gradeAwarded;
    }

    public String getGradeAwarded() {
        return gradeAwarded;
    }

    public void setGradeAwarded(String gradeAwarded) {
        this.gradeAwarded = gradeAwarded;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getYearTaken() {
        return yearTaken;
    }

    public void setYearTaken(String yearTaken) {
        this.yearTaken = yearTaken;
    }

    public String getSemesterTaken() {
        return semesterTaken;
    }

    public void setSemesterTaken(String semesterTaken) {
        this.semesterTaken = semesterTaken;
    }

}
