/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Student;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface StudentInterface {

    public List<Student> getStudent();

    public void createStudent(Student student);

    public void updateStudent(Student student);

    public void deleteStudent(Student student);
}
