/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Committee;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface CommitteeInterface {
    
    public List<Committee> getCommittee();

    public void createCommittee(Committee committee);

    public void updateCommittee(Committee committee);

    public void deleteCommittee(Committee committee);
}
