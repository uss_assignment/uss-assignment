/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Faculty;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface FacultyInterface {
    public List<Faculty> getFaculties();
    public void createFaculty(Faculty faculty);
    public void updateFaculty(Faculty faculty);
    public void deleteFaculty(Faculty faculty);
}
    

