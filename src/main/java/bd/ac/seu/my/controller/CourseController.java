/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.ProrammeDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.Programme;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class CourseController implements Initializable {

    @FXML
    private TextField titleField;
    @FXML
    private TextField codeField;
    @FXML
    private TableView<Course> courseTable;
    @FXML
    private TableColumn<Course, String> codeColumn;
    @FXML
    private TableColumn<Course, String> titleColumn;
    @FXML
    private TableColumn<Course, String> programmeColumn;
    @FXML
    private ComboBox<String> programmeDropdown;

    private ObservableList<Course> observableList;
    private List<Programme> programmeList = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CourseDAO courseDAO = new CourseDAO();
        ProrammeDAO prorammeDAO = new ProrammeDAO();
        observableList = FXCollections.observableArrayList(courseDAO.getCourse());
        courseTable.setItems(observableList);

        codeColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCourseCode()));
        titleColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCourseTitle()));
        programmeColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getProgramme().getProgrammeCode()));

        programmeList = prorammeDAO.getProramme();
        List<String> programmeNameList = new ArrayList<>();
        programmeList.forEach((programme) -> {
            programmeNameList.add(programme.getProgrammeCode());
        });
        programmeDropdown.getItems().addAll(programmeNameList);

    }

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String courseCode = codeField.getText();
        String courseTitle = titleField.getText();

        String programmeName = programmeDropdown.getSelectionModel().getSelectedItem();

        if (courseCode.equals("") || courseTitle.equals("") || programmeName == null || programmeName.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Programme programme = null;
            for (Programme prog : programmeList) {
                if (prog.getProgrammeCode().equals(programmeName)) {
                    programme = prog;
                }
            }

            Course course = new Course(courseCode, courseTitle, programme);
            CourseDAO courseDAO = new CourseDAO();
            courseDAO.createCourse(course);
            observableList.clear();
            observableList.addAll(courseDAO.getCourse());

            codeField.setText("");
            titleField.setText("");
            programmeDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = courseTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String courseCode = courseTable.getItems().get(row).getCourseCode();
        String courseTitle = courseTable.getItems().get(row).getCourseTitle();
        String programmeName = courseTable.getItems().get(row).getProgramme().getProgrammeCode();

        codeField.setText(courseCode);
        titleField.setText(courseTitle);
        programmeDropdown.getSelectionModel().select(programmeName);

    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = courseTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String courseCode = courseTable.getItems().get(row).getCourseCode();
        String courseTitle = courseTable.getItems().get(row).getCourseTitle();
        String programmeName = courseTable.getItems().get(row).getProgramme().getProgrammeCode();

        Programme programme = null;
        for (Programme prog : programmeList) {
            if (prog.getProgrammeCode().equals(programmeName)) {
                programme = prog;
            }
        }

        Course course = new Course(courseCode, courseTitle, programme);
        CourseDAO courseDAO = new CourseDAO();
        courseDAO.deleteCourse(course);
        observableList.clear();
        observableList.addAll(courseDAO.getCourse());

        codeField.setText("");
        titleField.setText("");
        programmeDropdown.getSelectionModel().selectFirst();

    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }

}
