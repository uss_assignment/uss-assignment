/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CommitteeDAO;
import bd.ac.seu.my.DAO.CommitteeLecturerDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.DAO.LecturerDAO;
import bd.ac.seu.my.model.Committee;
import bd.ac.seu.my.model.CommitteeLecturer;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.Lecturer;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class CommitteeLectuterController implements Initializable {

    @FXML
    private TableView<CommitteeLecturer> committeeAssignTable;
    @FXML
    private TableColumn<CommitteeLecturer, String> lecturerColumn;
    @FXML
    private TableColumn<CommitteeLecturer, String> committeeColumn;
    @FXML
    private TableColumn<CommitteeLecturer, String> facultyColumn;
    @FXML
    private ComboBox<String> committeeDropdown;
    @FXML
    private ComboBox<String> lecturerDropdown;
    @FXML
    private ComboBox<String> facultyDropdown;

    private ObservableList<CommitteeLecturer> observableList;
    private List<Lecturer> lecturerList = new ArrayList<>();
    private List<Committee> committeeList = new ArrayList<>();
    private List<Faculty> facultyList = new ArrayList<>();


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CommitteeLecturerDAO committeeLecturerDAO = new CommitteeLecturerDAO();
        LecturerDAO lecturerDAO = new LecturerDAO();
        CommitteeDAO committeeDAO = new CommitteeDAO();
        FacultyDAO facultyDAO = new FacultyDAO();
        observableList = FXCollections.observableArrayList(committeeLecturerDAO.getCommittiLecturer());
        committeeAssignTable.setItems(observableList);

        lecturerColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getLecturer().getLecturereName()));
        committeeColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCommittee().getCommitteeTitle()));
        facultyColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getFaculty().getFacultyName()));

        lecturerList = lecturerDAO.getLecturers();
        List<String> lecturerNameList = new ArrayList<>();
        lecturerList.forEach((lecturer) -> {
            lecturerNameList.add(lecturer.getLecturereName());
        });
        lecturerDropdown.getItems().addAll(lecturerNameList);

        committeeList = committeeDAO.getCommittee();
        List<String> committeeNameList = new ArrayList<>();
        committeeList.forEach((committee) -> {
            committeeNameList.add(committee.getCommitteeTitle());
        });
        committeeDropdown.getItems().addAll(committeeNameList);

        facultyList = facultyDAO.getFaculties();
        List<String> facultyNameList = new ArrayList<>();
        facultyList.forEach((faculty) -> {
            facultyNameList.add(faculty.getFacultyName());
        });
        facultyDropdown.getItems().addAll(facultyNameList);
    }    

    @FXML
    private void submitButtonAction(ActionEvent event) {
        
        String lecturerName = lecturerDropdown.getSelectionModel().getSelectedItem();
        String committeeTitle = committeeDropdown.getSelectionModel().getSelectedItem();
        String facultyName = facultyDropdown.getSelectionModel().getSelectedItem();

        if (lecturerName == null || lecturerName.equals("") || committeeTitle == null || committeeTitle.equals("") || facultyName == null || facultyName.equals("") ) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            
            
            Faculty faculty = null;
            for (Faculty fac : facultyList) {
                if (fac.getFacultyName().equals(facultyName)) {
                    faculty = fac;
                }
            }

            Lecturer lecturer = null;
            for (Lecturer lec : lecturerList) {
                if (lec.getLecturereName().equals(lecturerName)) {
                    lecturer = lec;
                }
            }

            Committee committee = null;
            for (Committee com : committeeList) {
                if (com.getCommitteeTitle().equals(committeeTitle)) {
                    committee = com;
                }
            }
            
            CommitteeLecturer committeeLecturer = new CommitteeLecturer(lecturer, committee, faculty);

            CommitteeLecturerDAO committeeLecturerDAO = new CommitteeLecturerDAO();
            committeeLecturerDAO.createCommittiLecturer(committeeLecturer);
            observableList.clear();
            observableList.addAll(committeeLecturerDAO.getCommittiLecturer());

            lecturerDropdown.getSelectionModel().selectFirst();
            committeeDropdown.getSelectionModel().selectFirst();
            facultyDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = committeeAssignTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String committeeTitle = committeeAssignTable.getItems().get(row).getCommittee().getCommitteeTitle();
        String lecturerName = committeeAssignTable.getItems().get(row).getLecturer().getLecturereName();
        String facultyName = committeeAssignTable.getItems().get(row).getFaculty().getFacultyName();

        committeeDropdown.getSelectionModel().select(committeeTitle);
        lecturerDropdown.getSelectionModel().select(lecturerName);
        facultyDropdown.getSelectionModel().select(facultyName);

    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = committeeAssignTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String committeeTitle = committeeAssignTable.getItems().get(row).getCommittee().getCommitteeTitle();
        String lecturerName = committeeAssignTable.getItems().get(row).getLecturer().getLecturereName();
        String facultyName = committeeAssignTable.getItems().get(row).getFaculty().getFacultyName();

        committeeDropdown.getSelectionModel().select(committeeTitle);
        lecturerDropdown.getSelectionModel().select(lecturerName);
        facultyDropdown.getSelectionModel().select(facultyName);
        
        Faculty faculty = null;
            for (Faculty fac : facultyList) {
                if (fac.getFacultyName().equals(facultyName)) {
                    faculty = fac;
                }
            }

            Lecturer lecturer = null;
            for (Lecturer lec : lecturerList) {
                if (lec.getLecturereName().equals(lecturerName)) {
                    lecturer = lec;
                }
            }

            Committee committee = null;
            for (Committee com : committeeList) {
                if (com.getCommitteeTitle().equals(committeeTitle)) {
                    committee = com;
                }
            }
            
            CommitteeLecturer committeeLecturer = new CommitteeLecturer(lecturer, committee, faculty);

            CommitteeLecturerDAO committeeLecturerDAO = new CommitteeLecturerDAO();
            committeeLecturerDAO.deleteCommittiLecturer(committeeLecturer);
            observableList.clear();
            observableList.addAll(committeeLecturerDAO.getCommittiLecturer());

            lecturerDropdown.getSelectionModel().selectFirst();
            committeeDropdown.getSelectionModel().selectFirst();
            facultyDropdown.getSelectionModel().selectFirst();

    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }
    
}
