/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.DAO.LecturerDAO;
import bd.ac.seu.my.DAO.SchoolDAO;
import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.Lecturer;
import bd.ac.seu.my.model.School;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class LecturerController implements Initializable {

    @FXML
    private TextField lecturerNameField;
    @FXML
    private TableView<Lecturer> lecturerTable;
    @FXML
    private TableColumn<Lecturer, String> idColumn;
    @FXML
    private TableColumn<Lecturer, String> schoolColumn;
    @FXML
    private TableColumn<Lecturer, String> supervisorColumn;
    @FXML
    private TableColumn<Lecturer, String> nameColumn;
    @FXML
    private TableColumn<Lecturer, String> titleColumn;
    @FXML
    private TableColumn<Lecturer, String> roomColumn;
    @FXML
    private ComboBox<String> schoolDropdown;
    @FXML
    private ComboBox<String> supervisorDropdown;
    @FXML
    private TextField lecturerTitleField;
    @FXML
    private TextField lecturerRoomField;

    private ObservableList<Lecturer> observableList;
    private List<School> schoolList = new ArrayList<>();
    private List<Lecturer> lecturerList = new ArrayList<>();
    private int staffId;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SchoolDAO schoolDAO = new SchoolDAO();
        LecturerDAO lecturerDAO = new LecturerDAO();
        
        List<Lecturer> lecList = lecturerDAO.getLecturers();
        /*
        for (Lecturer lecturer : lecList) {
            if (lecturer.getSupervisor() == null) {
                Lecturer lec = new Lecturer(lecturer.getSchool(), lecturer, lecturer.getLecturereName(), lecturer.getLecturertitle(), lecturer.getOfficeRoom());
                lecturerDAO.updateLecturer(lec);
            }
        }
        */
        observableList = FXCollections.observableArrayList(lecList);
        lecturerTable.setItems(observableList);

        idColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getStaffId() + ""));
        schoolColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSchool().getSchoolName()));
        supervisorColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSupervisor().getLecturereName()));
        nameColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getLecturereName()));
        titleColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getLecturertitle()));
        roomColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getOfficeRoom()));

        schoolList = schoolDAO.getSchools();
        List<String> schoolNameList = new ArrayList<>();
        schoolList.forEach((school) -> {
            schoolNameList.add(school.getSchoolName());
        });
        schoolDropdown.getItems().addAll(schoolNameList);

        lecturerList = lecturerDAO.getLecturers();
        List<String> lecturerNameList = new ArrayList<>();
        lecturerList.forEach((lecturer) -> {
            lecturerNameList.add(lecturer.getLecturereName());
        });
        supervisorDropdown.getItems().addAll(lecturerNameList);
    }

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String lecturereName = lecturerNameField.getText();
        String lecturertitle = lecturerTitleField.getText();
        String officeRoom = lecturerRoomField.getText();

        String schoolName = schoolDropdown.getSelectionModel().getSelectedItem();
        String supervisorName = supervisorDropdown.getSelectionModel().getSelectedItem();
        
        
            
        if (lecturereName.equals("") || lecturertitle.equals("") || officeRoom.equals("") || schoolName == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Lecturer supervisor = null;
            for (Lecturer lec : lecturerList) {
                if (lec.getLecturereName().equals(supervisorName)) {
                    supervisor = lec;
                }
            }
            School school = null;
            for (School sc : schoolList) {
                if (sc.getSchoolName().equals(schoolName)) {
                    school = sc;
                }
            }
                
            Lecturer lecturer = new Lecturer(staffId, school, supervisor, lecturereName, lecturertitle, officeRoom);
           
            LecturerDAO lecturerDAO = new LecturerDAO();
            lecturerDAO.createLecturer(lecturer);
            observableList.clear();
            observableList.addAll(lecturerDAO.getLecturers());

            lecturerNameField.setText("");
            lecturerTitleField.setText("");
            lecturerRoomField.setText("");
            schoolDropdown.getSelectionModel().selectFirst();
            supervisorDropdown.getSelectionModel().selectFirst();
            staffId = 0;
        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {

        TablePosition firstCell = lecturerTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        staffId = lecturerTable.getItems().get(row).getStaffId();
        String lecturereName = lecturerTable.getItems().get(row).getLecturereName();
        String lecturertitle = lecturerTable.getItems().get(row).getLecturertitle();
        String officeRoom = lecturerTable.getItems().get(row).getOfficeRoom();
        String schoolName = lecturerTable.getItems().get(row).getSchool().getSchoolName();
        String supervisorName = lecturerTable.getItems().get(row).getSupervisor().getLecturereName();

        lecturerNameField.setText(lecturereName);
        lecturerTitleField.setText(lecturertitle);
        lecturerRoomField.setText(officeRoom);
        schoolDropdown.getSelectionModel().select(schoolName);
        supervisorDropdown.getSelectionModel().select(supervisorName);

    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {

        TablePosition firstCell = lecturerTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        int staffsId = lecturerTable.getItems().get(row).getStaffId();
        String lecturereName = lecturerTable.getItems().get(row).getLecturereName();
        String lecturertitle = lecturerTable.getItems().get(row).getLecturertitle();
        String officeRoom = lecturerTable.getItems().get(row).getOfficeRoom();
        String schoolName = lecturerTable.getItems().get(row).getSchool().getSchoolName();
        String supervisorName = lecturerTable.getItems().get(row).getSupervisor().getLecturereName();

        Lecturer supervisor = null;
        for (Lecturer lec : lecturerList) {
            if (lec.getLecturereName().equals(lecturereName)) {
                supervisor = lec;
            }
        }

        School school = null;
        for (School sc : schoolList) {
            if (sc.getSchoolName().equals(schoolName)) {
                school = sc;
            }
        }

        Lecturer lecturer = new Lecturer(staffsId, school, supervisor, lecturereName, lecturertitle, officeRoom);

        LecturerDAO lecturerDAO = new LecturerDAO();
        lecturerDAO.deleteLecturer(lecturer);
        observableList.clear();
        observableList.addAll(lecturerDAO.getLecturers());

        lecturerNameField.setText("");
        lecturerTitleField.setText("");
        lecturerRoomField.setText("");
        schoolDropdown.getSelectionModel().selectFirst();
        supervisorDropdown.getSelectionModel().selectFirst();
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }

}
