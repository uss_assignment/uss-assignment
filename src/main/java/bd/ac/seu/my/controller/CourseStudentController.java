/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.CourseStudentDAO;
import bd.ac.seu.my.DAO.StudentDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.CourseStudent;
import bd.ac.seu.my.model.Student;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class CourseStudentController implements Initializable {

    @FXML
    private TableView<CourseStudent> courseAssignTable;
    @FXML
    private TableColumn<CourseStudent, String> courseColumn;
    @FXML
    private TableColumn<CourseStudent, String> studentColumn;
    @FXML
    private TableColumn<CourseStudent, String> semesterColumn;
    @FXML
    private TableColumn<CourseStudent, String> yearColumn;
    @FXML
    private TableColumn<CourseStudent, String> gradeColumn;
    @FXML
    private ComboBox<String> studentDropdown;
    @FXML
    private ComboBox<String> courseDropdown;
    @FXML
    private TextField yearField;
    @FXML
    private TextField semesterField;
    @FXML
    private TextField gradeField;

    private ObservableList<CourseStudent> observableList;
    private List<Student> studentList = new ArrayList<>();
    private List<Course> courseList = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CourseStudentDAO courseStudentDAO = new CourseStudentDAO();
        CourseDAO courseDAO = new CourseDAO();
        StudentDAO studentDAO = new StudentDAO();
        observableList = FXCollections.observableArrayList(courseStudentDAO.getCourseStudent());
        courseAssignTable.setItems(observableList);

        courseColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCourse().getCourseCode()));
        studentColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getStudent().getStudentId() + ""));
        semesterColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSemesterTaken()));
        yearColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getYearTaken()));
        gradeColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getGradeAwarded()));

        courseList = courseDAO.getCourse();
        List<String> courseNameList = new ArrayList<>();
        courseList.forEach((course) -> {
            courseNameList.add(course.getCourseCode());
        });
        courseDropdown.getItems().addAll(courseNameList);

        studentList = studentDAO.getStudent();
        List<String> studentIdList = new ArrayList<>();
        studentList.forEach((student) -> {
            studentIdList.add(student.getStudentId() + "");
        });
        studentDropdown.getItems().addAll(studentIdList);

    }

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String yearTaken = yearField.getText();
        String semesterTaken = semesterField.getText();
        String gradeAwarded = gradeField.getText();
        String courseName = courseDropdown.getSelectionModel().getSelectedItem();
        String studentId = studentDropdown.getSelectionModel().getSelectedItem();

        if (courseName == null || studentId == null || courseName.equals("") || studentId.equals("") || yearTaken.equals("") || semesterTaken.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {

            Course course = null;
            for (Course co : courseList) {
                if (co.getCourseCode().equals(courseName)) {
                    course = co;
                }
            }

            Student student = null;
            for (Student stu : studentList) {
                if (stu.getStudentId() == Integer.parseInt(studentId)) {
                    student = stu;
                }
            }

            CourseStudent courseStudent = new CourseStudent(course, student, yearTaken, semesterTaken, gradeAwarded);

            CourseStudentDAO courseStudentDAO = new CourseStudentDAO();
            courseStudentDAO.createCourseStudent(courseStudent);
            observableList.clear();
            observableList.addAll(courseStudentDAO.getCourseStudent());

            courseDropdown.getSelectionModel().selectFirst();
            studentDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = courseAssignTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String courseName = courseAssignTable.getItems().get(row).getCourse().getCourseCode();
        String studentID = courseAssignTable.getItems().get(row).getStudent().getStudentId() + "";
        String semesterTaken = courseAssignTable.getItems().get(row).getSemesterTaken();
        String yearTaken = courseAssignTable.getItems().get(row).getYearTaken();
        String gradeAwarded = courseAssignTable.getItems().get(row).getGradeAwarded();

        yearField.setText(yearTaken);
        semesterField.setText(semesterTaken);
        gradeField.setText(gradeAwarded);
        courseDropdown.getSelectionModel().select(courseName);
        studentDropdown.getSelectionModel().select(studentID);
    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = courseAssignTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String courseName = courseAssignTable.getItems().get(row).getCourse().getCourseCode();
        String studentId = courseAssignTable.getItems().get(row).getStudent().getStudentName();
        String semesterTaken = courseAssignTable.getItems().get(row).getSemesterTaken();
        String yearTaken = courseAssignTable.getItems().get(row).getYearTaken();
        String gradeAwarded = courseAssignTable.getItems().get(row).getGradeAwarded();

        Course course = null;
        for (Course co : courseList) {
            if (co.getCourseCode().equals(courseName)) {
                course = co;
            }
        }

        Student student = null;
        for (Student stu : studentList) {
            if (stu.getStudentId() == Integer.parseInt(studentId)) {
                student = stu;
            }
        }

        CourseStudent courseStudent = new CourseStudent(course, student, yearTaken, semesterTaken, gradeAwarded);

        CourseStudentDAO courseStudentDAO = new CourseStudentDAO();
        courseStudentDAO.deleteCourseStudent(courseStudent);
        observableList.clear();
        observableList.addAll(courseStudentDAO.getCourseStudent());

        courseDropdown.getSelectionModel().selectFirst();
        studentDropdown.getSelectionModel().selectFirst();

    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }

}
