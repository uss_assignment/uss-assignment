/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CommitteeDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.model.Committee;
import bd.ac.seu.my.model.Faculty;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class CommitteeController implements Initializable {

    @FXML
    private TextField committeeTitleField;
    @FXML
    private TableView<Committee> committeeTable;
    @FXML
    private TableColumn<Committee, String> titleColumn;
    @FXML
    private TableColumn<Committee, String> facultyColumn;
    @FXML
    private TableColumn<Committee, String> meetingFrequencyColumn;
    @FXML
    private ComboBox<String> facultyDropdown;
    @FXML
    private TextField meetingFrequencyField;

    private ObservableList<Committee> observableList;
    private List<Faculty> facultyList = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CommitteeDAO committeeDAO = new CommitteeDAO();
        FacultyDAO facultyDAO = new FacultyDAO();
        observableList = FXCollections.observableArrayList(committeeDAO.getCommittee());
        committeeTable.setItems(observableList);

        titleColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCommitteeTitle()));
        facultyColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getFaculty().getFacultyName()));
        meetingFrequencyColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getMeetingFrequency()));

        facultyList = facultyDAO.getFaculties();
        List<String> facultyNameList = new ArrayList<>();
        facultyList.forEach((faculty) -> {
            facultyNameList.add(faculty.getFacultyName());
        });
        facultyDropdown.getItems().addAll(facultyNameList);
    }

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String committeeTitle = committeeTitleField.getText();
        String meetingFrequency = meetingFrequencyField.getText();

        String facultyName = facultyDropdown.getSelectionModel().getSelectedItem();

        if (committeeTitle.equals("") || meetingFrequency.equals("") || facultyName == null || facultyName.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Faculty faculty = null;
            for (Faculty fac : facultyList) {
                if (fac.getFacultyName().equals(facultyName)) {
                    faculty = fac;
                }
            }

            Committee committee = new Committee(committeeTitle, faculty, meetingFrequency);
            CommitteeDAO committeeDAO = new CommitteeDAO();
            committeeDAO.createCommittee(committee);
            observableList.clear();
            observableList.addAll(committeeDAO.getCommittee());

            committeeTitleField.setText("");
            meetingFrequencyField.setText("");
            facultyDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = committeeTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String committeeTitle = committeeTable.getItems().get(row).getCommitteeTitle();
        String meetingFrequency = committeeTable.getItems().get(row).getMeetingFrequency();
        String facultyName = committeeTable.getItems().get(row).getFaculty().getFacultyName();

        committeeTitleField.setText(committeeTitle);
        meetingFrequencyField.setText(meetingFrequency);
        facultyDropdown.getSelectionModel().select(facultyName);

    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = committeeTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String committeeTitle = committeeTable.getItems().get(row).getCommitteeTitle();
        String meetingFrequency = committeeTable.getItems().get(row).getMeetingFrequency();
        String facultyName = committeeTable.getItems().get(row).getFaculty().getFacultyName();

        Faculty faculty = null;
        for (Faculty fac : facultyList) {
            if (fac.getFacultyName().equals(facultyName)) {
                faculty = fac;
            }
        }
        Committee committee = new Committee(committeeTitle, faculty, meetingFrequency);
        CommitteeDAO committeeDAO = new CommitteeDAO();
        committeeDAO.deleteCommittee(committee);
        observableList.clear();
        observableList.addAll(committeeDAO.getCommittee());

        committeeTitleField.setText("");
        meetingFrequencyField.setText("");
        facultyDropdown.getSelectionModel().selectFirst();
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }

}
