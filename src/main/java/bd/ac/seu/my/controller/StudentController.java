/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.ProrammeDAO;
import bd.ac.seu.my.DAO.StudentDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.Programme;
import bd.ac.seu.my.model.Student;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class StudentController implements Initializable {

    @FXML
    private TextField idField;
    @FXML
    private TextField birthDayField;
    @FXML
    private TableView<Student> studentTable;
    @FXML
    private TableColumn<Student, String> idColumn;
    @FXML
    private TableColumn<Student, String> nameColumn;
    @FXML
    private TableColumn<Student, String> programmeColumn;
    @FXML
    private TableColumn<Student, String> birthdayColumn;
    @FXML
    private TableColumn<Student, String> entrolmentColumn;
    @FXML
    private ComboBox<String> programmeDropdown;
    @FXML
    private TextField yearField;
    @FXML
    private TextField nameField;
    
    private ObservableList<Student> observableList;
    private List<Programme> programmeList = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        StudentDAO studentDAO = new StudentDAO();
        ProrammeDAO prorammeDAO = new ProrammeDAO();
        observableList = FXCollections.observableArrayList(studentDAO.getStudent());
        studentTable.setItems(observableList);

        idColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getStudentId() + ""));
        nameColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getStudentName()));
        programmeColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getProgramme().getProgrammeCode()));
        birthdayColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getStudentBirthDay()));
        entrolmentColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getYearEnroll()));
        
        programmeList = prorammeDAO.getProramme();
        List<String> programmeNameList = new ArrayList<>();
        programmeList.forEach((programme) -> {
            programmeNameList.add(programme.getProgrammeCode());
        });
        programmeDropdown.getItems().addAll(programmeNameList);

    }    

    @FXML
    private void submitButtonAction(ActionEvent event) {
        int studentId = Integer.parseInt(idField.getText());
        String studentName = nameField.getText();
        String studentBirthDay = birthDayField.getText();
        String yearEnroll = yearField.getText();

        String programmeName = programmeDropdown.getSelectionModel().getSelectedItem();

        if (studentName.equals("") || studentBirthDay.equals("") || yearEnroll.equals("") || programmeName == null || programmeName.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Programme programme = null;
            for (Programme prog : programmeList) {
                if (prog.getProgrammeCode().equals(programmeName)) {
                    programme = prog;
                }
            }

            Student student = new Student(studentId, programme, studentName, studentBirthDay, yearEnroll);
            StudentDAO studentDAO = new StudentDAO();
            studentDAO.createStudent(student);
            observableList.clear();
            observableList.addAll(studentDAO.getStudent());

            idField.setText("");
            nameField.setText("");
            birthDayField.setText("");
            yearField.setText("");
            programmeDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = studentTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String studentId = studentTable.getItems().get(row).getStudentId() + "";
        String studentName = studentTable.getItems().get(row).getStudentName();
        String programmeName = studentTable.getItems().get(row).getProgramme().getProgrammeCode();
        String studentBirthDay = studentTable.getItems().get(row).getStudentBirthDay();
        String yearEnroll = studentTable.getItems().get(row).getYearEnroll();

        idField.setText(studentId);
        nameField.setText(studentName);
        birthDayField.setText(studentBirthDay);
        yearField.setText(yearEnroll);
        programmeDropdown.getSelectionModel().select(programmeName);
    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = studentTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        int studentId = Integer.parseInt(studentTable.getItems().get(row).getStudentId() + "");
        String studentName = studentTable.getItems().get(row).getStudentName();
        String programmeName = studentTable.getItems().get(row).getProgramme().getProgrammeCode();
        String studentBirthDay = studentTable.getItems().get(row).getStudentBirthDay();
        String yearEnroll = studentTable.getItems().get(row).getYearEnroll();

         Programme programme = null;
            for (Programme prog : programmeList) {
                if (prog.getProgrammeCode().equals(programmeName)) {
                    programme = prog;
                }
            }

            Student student = new Student(studentId, programme, studentName, studentBirthDay, yearEnroll);
            StudentDAO studentDAO = new StudentDAO();
            studentDAO.deleteStudent(student);
            observableList.clear();
            observableList.addAll(studentDAO.getStudent());

            idField.setText("");
            nameField.setText("");
            birthDayField.setText("");
            yearField.setText("");
            programmeDropdown.getSelectionModel().selectFirst();
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }
    
}
