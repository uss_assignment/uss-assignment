/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.model.Campus;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class CampusController implements Initializable {

    @FXML
    private TextField campusAddressField;
    @FXML
    private TextField campusNameField;
    @FXML
    private TextField campusDistanceField;
    @FXML
    private TextField campusBusField;
    @FXML
    private TableView<Campus> campusTable;
    @FXML
    private TableColumn<Campus, String> nameColumn;
    @FXML
    private TableColumn<Campus, String> addressColumn;
    @FXML
    private TableColumn<Campus, String> distanceColumn;
    @FXML
    private TableColumn<Campus, String> busColumn;

    private ObservableList<Campus> observableList;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        CampusDAO campusDAO = new CampusDAO();
        observableList = FXCollections.observableArrayList(campusDAO.getCampuses());
        campusTable.setItems(observableList);

        nameColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCampusName()));
        addressColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCampusAddress()));
        distanceColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCampusDistance()));
        busColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getBusNo()));
    }

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String campusName = campusNameField.getText();
        String campusAddress = campusAddressField.getText();
        String campusDistance = campusDistanceField.getText();
        String busNo = campusBusField.getText();

        if (campusName.equals("") || campusAddress.equals("") || campusDistance.equals("") || busNo.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Campus campus = new Campus(campusName, campusAddress, campusDistance, busNo);
            CampusDAO campusDAO = new CampusDAO();
            campusDAO.createCampus(campus);
            observableList.clear();
            observableList.addAll(campusDAO.getCampuses());

            campusNameField.setText("");
            campusAddressField.setText("");
            campusDistanceField.setText("");
            campusBusField.setText("");
        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = campusTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String campusName = campusTable.getItems().get(row).getCampusName();
        String campusAddress = campusTable.getItems().get(row).getCampusAddress();
        String campusDistance = campusTable.getItems().get(row).getCampusDistance();
        String busNo = campusTable.getItems().get(row).getBusNo();

        campusNameField.setText(campusName);
        campusAddressField.setText(campusAddress);
        campusDistanceField.setText(campusDistance);
        campusBusField.setText(busNo);
    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = campusTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String campusName = campusTable.getItems().get(row).getCampusName();
        String campusAddress = campusTable.getItems().get(row).getCampusAddress();
        String campusDistance = campusTable.getItems().get(row).getCampusDistance();
        String busNo = campusTable.getItems().get(row).getBusNo();

        Campus campus = new Campus(campusName, campusAddress, campusDistance, busNo);
        CampusDAO campusDAO = new CampusDAO();
        campusDAO.deleteCampus(campus);
        observableList.clear();
        observableList.addAll(campusDAO.getCampuses());

        campusNameField.setText("");
        campusAddressField.setText("");
        campusDistanceField.setText("");
        campusBusField.setText("");
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }

}
