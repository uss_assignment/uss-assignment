/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.ClubDAO;
import bd.ac.seu.my.DAO.SportsDAO;
import bd.ac.seu.my.model.Club;
import bd.ac.seu.my.model.Sport;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class SportController implements Initializable {

    @FXML
    private TableView<Sport> sportsTable;
    @FXML
    private TableColumn<Sport, String> clubColumn;
    @FXML
    private TableColumn<Sport, String> sportsColumn;
    @FXML
    private ComboBox<String> clubDropdown;
    @FXML
    private TextField sportsNameField;

    private ObservableList<Sport> observableList;
    private List<Club> clubList = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SportsDAO sportsDAO = new SportsDAO();
        ClubDAO clubDAO = new ClubDAO();
        observableList = FXCollections.observableArrayList(sportsDAO.getSports());
        sportsTable.setItems(observableList);

        clubColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getClub().getClubName()));
        sportsColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSportName()));

        clubList = clubDAO.getClub();
        List<String> clubNameList = new ArrayList<>();
        clubList.forEach((club) -> {
            clubNameList.add(club.getClubName());
        });
        clubDropdown.getItems().addAll(clubNameList);
    }

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String sportName = sportsNameField.getText();

        String clubName = clubDropdown.getSelectionModel().getSelectedItem();

        if (sportName.equals("") || clubDropdown.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {

            Club club = null;
            for (Club cl : clubList) {
                if (cl.getClubName().equals(clubName)) {
                    club = cl;
                }
            }

            Sport sport = new Sport(sportName, club);

            SportsDAO sportsDAO = new SportsDAO();
            sportsDAO.createSports(sport);
            observableList.clear();
            observableList.addAll(sportsDAO.getSports());

            sportsNameField.setText("");
            clubDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = sportsTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String sportName = sportsTable.getItems().get(row).getSportName();
        String clubName = sportsTable.getItems().get(row).getClub().getClubName();

        sportsNameField.setText(sportName);
        clubDropdown.getSelectionModel().select(clubName);

    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {

        TablePosition firstCell = sportsTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String sportName = sportsTable.getItems().get(row).getSportName();
        String clubName = sportsTable.getItems().get(row).getClub().getClubName();

        Club club = null;
        for (Club cl : clubList) {
            if (cl.getClubName().equals(clubName)) {
                club = cl;
            }
        }

        Sport sport = new Sport(sportName, club);
        SportsDAO sportsDAO = new SportsDAO();
        sportsDAO.deleteSports(sport);
        observableList.clear();
        observableList.addAll(sportsDAO.getSports());

        sportsNameField.setText("");
        clubDropdown.getSelectionModel().selectFirst();
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }

}
