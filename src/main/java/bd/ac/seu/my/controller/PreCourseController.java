/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.PreCourseDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.PreCourse;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class PreCourseController implements Initializable {

    @FXML
    private TableView<PreCourse> precourseAssignTable;
    @FXML
    private TableColumn<PreCourse, String> courseColumn;
    @FXML
    private TableColumn<PreCourse, String> precourseColumn;
    @FXML
    private ComboBox<String> preCourseDropdown;
    @FXML
    private ComboBox<String> courseDropdown;

    private ObservableList<PreCourse> observableList;
    private List<Course> courseList = new ArrayList<>();
    private List<Course> preCourseList = new ArrayList<>();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CourseDAO courseDAO = new CourseDAO();
        PreCourseDAO preCourseDAO = new PreCourseDAO();
        observableList = FXCollections.observableArrayList(preCourseDAO.getPreCourse());
        precourseAssignTable.setItems(observableList);

        courseColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCourse().getCourseCode()));
        precourseColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCourse().getCourseCode()));

        courseList = courseDAO.getCourse();
        List<String> courseNameList = new ArrayList<>();
        courseList.forEach((course) -> {
            courseNameList.add(course.getCourseCode());
        });
        courseDropdown.getItems().addAll(courseNameList);
        
        preCourseList = courseDAO.getCourse();
        List<String> preCourseNameList = new ArrayList<>();
        preCourseList.forEach((precourse) -> {
            preCourseNameList.add(precourse.getCourseCode());
        });
        preCourseDropdown.getItems().addAll(preCourseNameList);

    }    

    @FXML
    private void submitButtonAction(ActionEvent event) {
        
        String courseName = courseDropdown.getSelectionModel().getSelectedItem();
        String preCourseName = preCourseDropdown.getSelectionModel().getSelectedItem();
        
        if (courseName == null || preCourseName == null || courseName.equals("") || preCourseName.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Course course = null;
            for (Course co : courseList) {
                if (co.getCourseCode().equals(courseName)) {
                    course = co;
                }
            }

            Course preCourse = null;
            for (Course co : preCourseList) {
                if (co.getCourseCode().equals(preCourseName)) {
                    course = co;
                }
            }
            
            PreCourse pc = new PreCourse(course, preCourse);

            PreCourseDAO preCourseDAO = new PreCourseDAO();
            preCourseDAO.createPreCourse(pc);
            observableList.clear();
            observableList.addAll(preCourseDAO.getPreCourse());

            courseDropdown.getSelectionModel().selectFirst();
            preCourseDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = precourseAssignTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String courseName = precourseAssignTable.getItems().get(row).getCourse().getCourseCode();
        String preCourseName = precourseAssignTable.getItems().get(row).getCourse().getCourseCode();

        courseDropdown.getSelectionModel().select(courseName);
        preCourseDropdown.getSelectionModel().select(preCourseName);
    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = precourseAssignTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String courseName = precourseAssignTable.getItems().get(row).getCourse().getCourseCode();
        String preCourseName = precourseAssignTable.getItems().get(row).getCourse().getCourseCode();

        Course course = null;
            for (Course co : courseList) {
                if (co.getCourseCode().equals(courseName)) {
                    course = co;
                }
            }

            Course preCourse = null;
            for (Course co : courseList) {
                if (co.getCourseCode().equals(preCourseName)) {
                    course = co;
                }
            }
            
            PreCourse pc = new PreCourse(course, preCourse);

            PreCourseDAO preCourseDAO = new PreCourseDAO();
            preCourseDAO.deletePreCourse(pc);
            observableList.clear();
            observableList.addAll(preCourseDAO.getPreCourse());

            courseDropdown.getSelectionModel().selectFirst();
            preCourseDropdown.getSelectionModel().selectFirst();
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }
    
}
