/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Faculty;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class FacultyController implements Initializable {

    @FXML
    private TextField deanNameField;
    @FXML
    private TextField facultyNameField;
    @FXML
    private TextField facultyBuildingField;
    @FXML
    private TableView<Faculty> facultyTable;
    @FXML
    private TableColumn<Faculty, String> nameColumn;
    @FXML
    private TableColumn<Faculty, String> deanColumn;
    @FXML
    private TableColumn<Faculty, String> buildingColumn;

    private ObservableList<Faculty> observableList;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        FacultyDAO facultyDAO = new FacultyDAO();
        observableList = FXCollections.observableArrayList(facultyDAO.getFaculties());
        facultyTable.setItems(observableList);

        nameColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getFacultyName()));
        deanColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getDeanName()));
        buildingColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getFacultyBuilding()));
    }    

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String facultyName = facultyNameField.getText();
        String deanName = deanNameField.getText();
        String facultyBuilding = facultyBuildingField.getText();

        if (facultyName.equals("") || deanName.equals("") || facultyBuilding.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Faculty faculty = new Faculty(facultyName, deanName, facultyBuilding);
            FacultyDAO facultyDAO = new FacultyDAO();
            facultyDAO.createFaculty(faculty);
            observableList.clear();
            observableList.addAll(facultyDAO.getFaculties());

            facultyNameField.setText("");
            deanNameField.setText("");
            facultyBuildingField.setText("");
        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = facultyTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String facultyName = facultyTable.getItems().get(row).getFacultyName();
        String deanName = facultyTable.getItems().get(row).getDeanName();
        String facultyBuilding = facultyTable.getItems().get(row).getFacultyBuilding();

        facultyNameField.setText(facultyName);
        deanNameField.setText(deanName);
        facultyBuildingField.setText(facultyBuilding);
    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = facultyTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String facultyName = facultyTable.getItems().get(row).getFacultyName();
        String deanName = facultyTable.getItems().get(row).getDeanName();
        String facultyBuilding = facultyTable.getItems().get(row).getFacultyBuilding();

        Faculty faculty = new Faculty(facultyName, deanName, facultyBuilding);
        FacultyDAO facultyDAO = new FacultyDAO();
        facultyDAO.deleteFaculty(faculty);
        observableList.clear();
        observableList.addAll(facultyDAO.getFaculties());

        facultyNameField.setText("");
        deanNameField.setText("");
        facultyBuildingField.setText("");
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }
    
}
