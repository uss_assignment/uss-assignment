/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.ProrammeDAO;
import bd.ac.seu.my.DAO.SchoolDAO;
import bd.ac.seu.my.model.Club;
import bd.ac.seu.my.model.Programme;
import bd.ac.seu.my.model.School;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class ProgrammeController implements Initializable {

    @FXML
    private TextField codeField;
    @FXML
    private TextField levelField;
    @FXML
    private TableView<Programme> programmeTable;
    @FXML
    private TableColumn<Programme, String> codeColumn;
    @FXML
    private TableColumn<Programme, String> schoolColumn;
    @FXML
    private TableColumn<Programme, String> titleColumn;
    @FXML
    private TableColumn<Programme, String> levelColumn;
    @FXML
    private TableColumn<Programme, String> lengthColumn;
    @FXML
    private ComboBox<String> schoolDropdown;
    @FXML
    private TextField titleField;
    @FXML
    private TextField lengthField;

    private ObservableList<Programme> observableList;
    private List<School> schoolList = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ProrammeDAO prorammeDAO = new ProrammeDAO();
        SchoolDAO schoolDAO = new SchoolDAO();
        observableList = FXCollections.observableArrayList(prorammeDAO.getProramme());
        programmeTable.setItems(observableList);

        codeColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getProgrammeCode()));
        schoolColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSchool().getSchoolName()));
        titleColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getProgrammeTitle()));
        levelColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getProgrammeLevel()));
        lengthColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getProgrammeLenth()));

        schoolList = schoolDAO.getSchools();
        List<String> schoolNameList = new ArrayList<>();
        schoolList.forEach((school) -> {
            schoolNameList.add(school.getSchoolName());
        });
        schoolDropdown.getItems().addAll(schoolNameList);

    }

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String programmeCode = codeField.getText();
        String programmeTitle = titleField.getText();
        String programmeLenth = lengthField.getText();
        String programmeLevel = levelField.getText();

        String schoolName = schoolDropdown.getSelectionModel().getSelectedItem();

        if (programmeCode.equals("") || programmeTitle.equals("") || programmeLenth.equals("") || programmeLevel.equals("") || schoolName == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {

            School school = null;
            for (School sc : schoolList) {
                if (sc.getSchoolName().equals(schoolName)) {
                    school = sc;
                }
            }

            Programme programme = new Programme(programmeCode, school, programmeTitle, programmeLevel, programmeLenth);

            ProrammeDAO prorammeDAO = new ProrammeDAO();
            prorammeDAO.createProramme(programme);
            observableList.clear();
            observableList.addAll(prorammeDAO.getProramme());

            codeField.setText("");
            titleField.setText("");
            lengthField.setText("");
            levelField.setText("");
            schoolDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = programmeTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String programmeCode = programmeTable.getItems().get(row).getProgrammeCode();
        String programmeTitle = programmeTable.getItems().get(row).getProgrammeTitle();
        String programmeLevel = programmeTable.getItems().get(row).getProgrammeLevel();
        String programmeLenth = programmeTable.getItems().get(row).getProgrammeLenth();
        String schoolName = programmeTable.getItems().get(row).getSchool().getSchoolName();

        codeField.setText(programmeCode);
        titleField.setText(programmeTitle);
        lengthField.setText(programmeLenth);
        levelField.setText(programmeLevel);
        schoolDropdown.getSelectionModel().select(schoolName);

    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = programmeTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String programmeCode = programmeTable.getItems().get(row).getProgrammeCode();
        String programmeTitle = programmeTable.getItems().get(row).getProgrammeTitle();
        String programmeLevel = programmeTable.getItems().get(row).getProgrammeLevel();
        String programmeLenth = programmeTable.getItems().get(row).getProgrammeLenth();
        String schoolName = programmeTable.getItems().get(row).getSchool().getSchoolName();

        School school = null;
        for (School sc : schoolList) {
            if (sc.getSchoolName().equals(schoolName)) {
                school = sc;
            }
        }
        
        Programme programme = new Programme(programmeCode, school, programmeTitle, programmeLevel, programmeLenth);

        ProrammeDAO prorammeDAO = new ProrammeDAO();
        prorammeDAO.createProramme(programme);

        observableList.clear();
        observableList.addAll(prorammeDAO.getProramme());

        codeField.setText("");
        titleField.setText("");
        lengthField.setText("");
        levelField.setText("");
        schoolDropdown.getSelectionModel().selectFirst();
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }

}
