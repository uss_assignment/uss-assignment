/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.DAO.SchoolDAO;
import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.School;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class SchoolController implements Initializable {

    @FXML
    private TextField schoolNameField;
    @FXML
    private TextField buildingField;
    @FXML
    private TableView<School> schoolTable;
    @FXML
    private TableColumn<School, String> nameColumn;
    @FXML
    private TableColumn<School, String> campusColumn;
    @FXML
    private TableColumn<School, String> facultyColumn;
    @FXML
    private TableColumn<School, String> buildingColumn;
    @FXML
    private ComboBox<String> campusDropdown;
    @FXML
    private ComboBox<String> facultyDropdown;

    private ObservableList<School> observableList;
    private List<Campus> campusList = new ArrayList<>();
    private List<Faculty> facultyList = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SchoolDAO schoolDAO = new SchoolDAO();
        CampusDAO campusDAO = new CampusDAO();
        FacultyDAO facultyDAO = new FacultyDAO();
        observableList = FXCollections.observableArrayList(schoolDAO.getSchools());
        schoolTable.setItems(observableList);

        nameColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSchoolName()));
        campusColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCampus().getCampusName()));
        facultyColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getFaculty().getFacultyName()));
        buildingColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSchoolBulding()));

        campusList = campusDAO.getCampuses();
        List<String> campusNameList = new ArrayList<>();
        campusList.forEach((campus) -> {
            campusNameList.add(campus.getCampusName());
        });
        campusDropdown.getItems().addAll(campusNameList);

        facultyList = facultyDAO.getFaculties();
        List<String> facultyNameList = new ArrayList<>();
        facultyList.forEach((faculty) -> {
            facultyNameList.add(faculty.getFacultyName());
        });
        facultyDropdown.getItems().addAll(facultyNameList);
    }

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String schoolName = schoolNameField.getText();
        String schoolBulding = buildingField.getText();

        String campusName = campusDropdown.getSelectionModel().getSelectedItem();
        String facultyName = facultyDropdown.getSelectionModel().getSelectedItem();

        if (schoolName.equals("") || schoolBulding.equals("") || campusName == null || facultyName == null || facultyName.equals("") || campusName.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Faculty faculty = null;
            for (Faculty fac : facultyList) {
                if (fac.getFacultyName().equals(facultyName)) {
                    faculty = fac;
                }
            }

            Campus campus = null;
            for (Campus cam : campusList) {
                if (cam.getCampusName().equals(campusName)) {
                    campus = cam;
                }
            }

            School school = new School(schoolName, faculty, campus, schoolBulding);

            SchoolDAO schoolDAO = new SchoolDAO();
            schoolDAO.createSchool(school);
            observableList.clear();
            observableList.addAll(schoolDAO.getSchools());

            schoolNameField.setText("");
            buildingField.setText("");
            campusDropdown.getSelectionModel().selectFirst();
            facultyDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = schoolTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String schoolName = schoolTable.getItems().get(row).getSchoolName();
        String schoolBulding = schoolTable.getItems().get(row).getSchoolBulding();
        String campusName = schoolTable.getItems().get(row).getCampus().getCampusName();
        String facultyName = schoolTable.getItems().get(row).getFaculty().getFacultyName();

        schoolNameField.setText(schoolName);
        buildingField.setText(schoolBulding);
        campusDropdown.getSelectionModel().select(campusName);
        facultyDropdown.getSelectionModel().select(facultyName);

    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {

        TablePosition firstCell = schoolTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String schoolName = schoolTable.getItems().get(row).getSchoolName();
        String schoolBulding = schoolTable.getItems().get(row).getSchoolBulding();
        String campusName = schoolTable.getItems().get(row).getCampus().getCampusName();
        String facultyName = schoolTable.getItems().get(row).getFaculty().getFacultyName();

        Faculty faculty = null;
        for (Faculty fac : facultyList) {
            if (fac.getFacultyName().equals(facultyName)) {
                faculty = fac;
            }
        }

        Campus campus = null;
        for (Campus cam : campusList) {
            if (cam.getCampusName().equals(campusName)) {
                campus = cam;
            }
        }

        School school = new School(schoolName, faculty, campus, schoolBulding);
        SchoolDAO schoolDAO = new SchoolDAO();
        schoolDAO.deleteSchool(school);
        observableList.clear();
        observableList.addAll(schoolDAO.getSchools());

        schoolNameField.setText("");
        buildingField.setText("");
        campusDropdown.getSelectionModel().selectFirst();
        facultyDropdown.getSelectionModel().selectFirst();
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }

}
