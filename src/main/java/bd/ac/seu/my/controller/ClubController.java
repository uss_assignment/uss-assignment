/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.DAO.ClubDAO;
import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Club;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class ClubController implements Initializable {

    @FXML
    private TextField nameField;
    @FXML
    private TextField phoneField;
    @FXML
    private TableView<Club> clubTable;
    @FXML
    private TableColumn<Club, String> nameColumn;
    @FXML
    private TableColumn<Club, String> campusColumn;
    @FXML
    private TableColumn<Club, String> buildingColumn;
    @FXML
    private TableColumn<Club, String> phoneColumn;
    @FXML
    private ComboBox<String> campusDropdown;
    @FXML
    private TextField buildingField;

    private ObservableList<Club> observableList;
    private List<Campus> campusList = new ArrayList<>();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CampusDAO campusDAO = new CampusDAO();
        ClubDAO clubDAO = new ClubDAO();
        observableList = FXCollections.observableArrayList(clubDAO.getClub());
        clubTable.setItems(observableList);

        nameColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getClubName()));
        campusColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCampus().getCampusName()));
        buildingColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getClubBuilding()));
        phoneColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getClubPhoneNumber()));
        
        
        campusList = campusDAO.getCampuses();
        List<String> campusNameList = new ArrayList<>();
        campusList.forEach((campus) -> {
            campusNameList.add(campus.getCampusName());
        });
        campusDropdown.getItems().addAll(campusNameList);
    }    

    @FXML
    private void submitButtonAction(ActionEvent event) {
        String clubName = nameField.getText();
        String clubBuilding = buildingField.getText();
        String clubPhoneNumber = phoneField.getText();

        String campusName = campusDropdown.getSelectionModel().getSelectedItem();

        if (clubName.equals("") || clubBuilding.equals("") || clubPhoneNumber.equals("") || campusName.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {

            Campus campus = null;
            for (Campus cam : campusList) {
                if (cam.getCampusName().equals(campusName)) {
                    campus = cam;
                }
            }

            Club club = new Club(clubName, campus, clubBuilding, clubPhoneNumber);

            ClubDAO clubDAO = new ClubDAO();
            clubDAO.createClub(club);
            observableList.clear();
            observableList.addAll(clubDAO.getClub());

            nameField.setText("");
            buildingField.setText("");
            phoneField.setText("");
            campusDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = clubTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String clubName = clubTable.getItems().get(row).getClubName();
        String clubBuilding = clubTable.getItems().get(row).getClubBuilding();
        String campusName = clubTable.getItems().get(row).getCampus().getCampusName();
        String clubPhoneNumber = clubTable.getItems().get(row).getClubPhoneNumber();

        nameField.setText(clubName);
        buildingField.setText(clubBuilding);
        phoneField.setText(clubPhoneNumber);
        campusDropdown.getSelectionModel().select(campusName);

    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        
        TablePosition firstCell = clubTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String clubName = clubTable.getItems().get(row).getClubName();
        String clubBuilding = clubTable.getItems().get(row).getClubBuilding();
        String campusName = clubTable.getItems().get(row).getCampus().getCampusName();
        String clubPhoneNumber = clubTable.getItems().get(row).getClubPhoneNumber();
        
        Campus campus = null;
        for (Campus cam : campusList) {
            if (cam.getCampusName().equals(campusName)) {
                campus = cam;
            }
        }

        Club club = new Club(clubName, campus, clubBuilding, clubPhoneNumber);
        ClubDAO clubDAO = new ClubDAO();
        clubDAO.deleteClub(club);
        observableList.clear();
        observableList.addAll(clubDAO.getClub());

        nameField.setText("");
        buildingField.setText("");
        phoneField.setText("");
        campusDropdown.getSelectionModel().selectFirst();
    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }
    
}
