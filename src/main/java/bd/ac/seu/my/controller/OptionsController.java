/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Club;
import bd.ac.seu.my.model.Committee;
import bd.ac.seu.my.model.CommitteeLecturer;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.CourseStudent;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.Lecturer;
import bd.ac.seu.my.model.LecturerCourse;
import bd.ac.seu.my.model.PreCourse;
import bd.ac.seu.my.model.Programme;
import bd.ac.seu.my.model.School;
import bd.ac.seu.my.model.Sport;
import bd.ac.seu.my.model.Student;
import bd.ac.seu.my.universitysystem.MainApp;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import bd.ac.seu.my.util.HibernateUtil;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class OptionsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        
        
        Campus campus = new Campus("AR Tower", "Banani", "5", "1");
        Campus campus2 = new Campus("Law Campus", "Banani", "4", "2");
        Club club = new Club("Computer Club", campus, "AR 1001", "01747942611");
        Club club2 = new Club("Debate Club", campus2, "Law 515", "01834525333");
        Sport sport = new Sport("Programming Contest", club);
        Sport sport2 = new Sport("Developing Contest", club);
        Sport sport3 = new Sport("Debate Contest", club2);
        Faculty faculty = new Faculty("CSE Department", "Tomal Sir", "1106");
        Faculty faculty2 = new Faculty("Law Department", "Shahriar Sir", "1105");
        School school = new School("Science School", faculty, campus, "AR Tower");
        School school2 = new School("Law School", faculty2, campus2, "Law Tower");
        Programme programme = new Programme("BSc. in CSE", school, "BSc. in Computer Science and Engineering", "BSc", "4th Year");
        Student student = new Student(programme, "Asaduzzaman Noor", "05-12-1994", "2017");
        Student student2 = new Student(programme, "Afsara Tasnim Mim", "01-01-1955", "2017");
        Lecturer lecturer = new Lecturer(school, null, "Tomal Sir", "Mr", "1101");
        Lecturer lecturer2 = new Lecturer(school, lecturer, "Shahriar Sir", "Mr", "1101");
        //lecturer.addSupervisor(lecturer2);
        Committee committee = new Committee("Computer Community", faculty, "Monthly");
        CommitteeLecturer committeeLecturer = new CommitteeLecturer(lecturer, committee);
        CommitteeLecturer committeeLecturer2 = new CommitteeLecturer(lecturer2, committee);
        Course course = new Course("CSE4027", "Advanced Java", programme);
        Course course2 = new Course("CSE4028", "Advanced Java Lab", programme);
        Course course3 = new Course("CSE2015", "Java", programme);
        Course course4 = new Course("CSE2016", "Java Lab", programme);
        PreCourse preCourse = new PreCourse(course, course3);
        PreCourse preCourse2 = new PreCourse(course2, course4);
        LecturerCourse lecturerCourse = new LecturerCourse(lecturer, course);
        LecturerCourse lecturerCourse2 = new LecturerCourse(lecturer, course2);
        LecturerCourse lecturerCourse3 = new LecturerCourse(lecturer2, course3);
        LecturerCourse lecturerCourse4 = new LecturerCourse(lecturer2, course4);
        CourseStudent courseStudent = new CourseStudent(course, student, "2017", "Spring", "A+");
        CourseStudent courseStudent2 = new CourseStudent(course2, student, "2017", "Spring", "A+");
        CourseStudent courseStudent3 = new CourseStudent(course3, student2, "2017", "Summer", "B+");
        CourseStudent courseStudent4 = new CourseStudent(course4, student2, "2017", "Summer", "C+");
        
        session.saveOrUpdate(campus);
        session.saveOrUpdate(campus2);
        session.saveOrUpdate(club);
        session.saveOrUpdate(club2);
        session.saveOrUpdate(sport);
        session.saveOrUpdate(sport2);
        session.saveOrUpdate(sport3);
        session.saveOrUpdate(faculty);
        session.saveOrUpdate(faculty2);
        session.saveOrUpdate(school);
        session.saveOrUpdate(school2);
        session.saveOrUpdate(programme);
        session.saveOrUpdate(student);
        session.saveOrUpdate(student2);
        session.saveOrUpdate(lecturer);
        session.saveOrUpdate(lecturer2);
        session.saveOrUpdate(committee);
        session.saveOrUpdate(committeeLecturer);
        session.saveOrUpdate(committeeLecturer2);
        session.saveOrUpdate(course);
        session.saveOrUpdate(course2);
        session.saveOrUpdate(course3);
        session.saveOrUpdate(course4);
        session.saveOrUpdate(preCourse);
        session.saveOrUpdate(preCourse2);
        session.saveOrUpdate(lecturerCourse);
        session.saveOrUpdate(lecturerCourse2);
        session.saveOrUpdate(lecturerCourse3);
        session.saveOrUpdate(lecturerCourse4);
        session.saveOrUpdate(courseStudent);
        session.saveOrUpdate(courseStudent2);
        session.saveOrUpdate(courseStudent3);
        session.saveOrUpdate(courseStudent4);
        transaction.commit();
        
        
        session.close();
        HibernateUtil.getSessionFactory().close();
        //System.err.println("Session Connected: " + session.isConnected());
*/
    }    

    @FXML
    private void campusButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Campus.fxml");
    }

    @FXML
    private void schoolButtonClick(ActionEvent event) {
        ChangeScene("/fxml/School.fxml");
    }

    @FXML
    private void facultyButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Faculty.fxml");
    }

    @FXML
    private void lecturerButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Lecturer.fxml");
    }

    @FXML
    private void committeeButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Committee.fxml");
    }

    @FXML
    private void assignLecturerToCommitteeButtonClick(ActionEvent event) {
        ChangeScene("/fxml/CommitteeLectuter.fxml");
    }

    @FXML
    private void programmeButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Programme.fxml");
    }

    @FXML
    private void courseButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Course.fxml");
    }

    @FXML
    private void assignPreCourseToCourseButtonClick(ActionEvent event) {
        ChangeScene("/fxml/PreCourse.fxml");
    }

    @FXML
    private void clubButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Club.fxml");
    }

    @FXML
    private void studentButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Student.fxml");
    }

    @FXML
    private void assignLecturertoCourseButtonClick(ActionEvent event) {
        ChangeScene("/fxml/LecturerCourse.fxml");
    }

    @FXML
    private void sportsButtonClick(ActionEvent event) {
        ChangeScene("/fxml/Sport.fxml");
    }

    @FXML
    private void assignStudentToCourseButtonClick(ActionEvent event) {
        ChangeScene("/fxml/CourseStudent.fxml");
    }
    
}
