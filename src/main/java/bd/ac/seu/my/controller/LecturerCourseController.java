/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.LecturerCourseDAO;
import bd.ac.seu.my.DAO.LecturerDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.Lecturer;
import bd.ac.seu.my.model.LecturerCourse;
import static bd.ac.seu.my.universitysystem.MainApp.ChangeScene;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asaduzzaman Noor
 */
public class LecturerCourseController implements Initializable {

    @FXML
    private TableView<LecturerCourse> committeeAssignTable;
    @FXML
    private TableColumn<LecturerCourse, String> courseColumn;
    @FXML
    private TableColumn<LecturerCourse, String> lectureColumn;
    @FXML
    private ComboBox<String> lecturerDropdown;
    @FXML
    private ComboBox<String> courseDropdown;

    private ObservableList<LecturerCourse> observableList;
    private List<Course> courseList = new ArrayList<>();
    private List<Lecturer> lecturerList = new ArrayList<>();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LecturerCourseDAO lecturerCourseDAO = new LecturerCourseDAO();
        CourseDAO courseDAO = new CourseDAO();
        LecturerDAO lecturerDAO = new LecturerDAO();
        observableList = FXCollections.observableArrayList(lecturerCourseDAO.getLecturerCourse());
        committeeAssignTable.setItems(observableList);

        courseColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getCourse().getCourseCode()));
        lectureColumn.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getLecturer().getLecturereName()));

        courseList = courseDAO.getCourse();
        List<String> courseNameList = new ArrayList<>();
        courseList.forEach((course) -> {
            courseNameList.add(course.getCourseCode());
        });
        courseDropdown.getItems().addAll(courseNameList);

        
        lecturerList = lecturerDAO.getLecturers();
        List<String> lecturerNameList = new ArrayList<>();
        lecturerList.forEach((lecturer) -> {
            lecturerNameList.add(lecturer.getLecturereName());
        });
        lecturerDropdown.getItems().addAll(lecturerNameList);

    }    

    @FXML
    private void submitButtonAction(ActionEvent event) {
        
        String courseName = courseDropdown.getSelectionModel().getSelectedItem();
        String lecturereName = lecturerDropdown.getSelectionModel().getSelectedItem();
        
        if (courseName == null || lecturereName == null || courseName.equals("") || lecturereName.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            alert.setTitle("Error");
            alert.setContentText("Fill up All Field");
            alert.showAndWait();
        } else {
            Course course = null;
            for (Course co : courseList) {
                if (co.getCourseCode().equals(courseName)) {
                    course = co;
                }
            }

            Lecturer lecturer = null;
            for (Lecturer lec : lecturerList) {
                if (lec.getLecturereName().equals(lecturereName)) {
                    lecturer = lec;
                }
            }
            
            LecturerCourse lecturerCourse = new LecturerCourse(lecturer, course);

            LecturerCourseDAO lecturerCourseDAO = new LecturerCourseDAO();
            lecturerCourseDAO.createLecturerCourse(lecturerCourse);
            observableList.clear();
            observableList.addAll(lecturerCourseDAO.getLecturerCourse());

            courseDropdown.getSelectionModel().selectFirst();
            lecturerDropdown.getSelectionModel().selectFirst();

        }
    }

    @FXML
    private void tableClicked(MouseEvent event) {
        TablePosition firstCell = committeeAssignTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String courseName = committeeAssignTable.getItems().get(row).getCourse().getCourseCode();
        String lecturereName = committeeAssignTable.getItems().get(row).getLecturer().getLecturereName();

        courseDropdown.getSelectionModel().select(courseName);
        lecturerDropdown.getSelectionModel().select(lecturereName);
    }

    @FXML
    private void deleteButtonAction(ActionEvent event) {
        TablePosition firstCell = committeeAssignTable.getSelectionModel().getSelectedCells().get(0);
        int row = firstCell.getRow();
        int column = firstCell.getColumn();

        String courseName = committeeAssignTable.getItems().get(row).getCourse().getCourseCode();
        String lecturereName = committeeAssignTable.getItems().get(row).getLecturer().getLecturereName();

        Course course = null;
            for (Course co : courseList) {
                if (co.getCourseCode().equals(courseName)) {
                    course = co;
                }
            }

            Lecturer lecturer = null;
            for (Lecturer lec : lecturerList) {
                if (lec.getLecturereName().equals(lecturereName)) {
                    lecturer = lec;
                }
            }
            
            LecturerCourse lecturerCourse = new LecturerCourse(lecturer, course);

            LecturerCourseDAO lecturerCourseDAO = new LecturerCourseDAO();
            lecturerCourseDAO.deleteLecturerCourse(lecturerCourse);
            observableList.clear();
            observableList.addAll(lecturerCourseDAO.getLecturerCourse());

            courseDropdown.getSelectionModel().selectFirst();
            lecturerDropdown.getSelectionModel().selectFirst();

    }

    @FXML
    private void homeButtonAction(ActionEvent event) {
        ChangeScene("/fxml/Options.fxml");
    }
    
}
