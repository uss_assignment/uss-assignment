package bd.ac.seu.my.universitysystem;

import bd.ac.seu.my.util.HibernateUtil;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.hibernate.Session;

public class MainApp extends Application {

    //=======Fixed Window Size Start========//    
    public static double origW;
    public static double origH;

    private static Stage mainStage;

    public static Stage getStage() {
        return mainStage;
    }
    //=======Fixed Window Size End========//    

    @Override
    public void start(Stage stage) throws Exception {

        mainStage = stage;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Options.fxml"));

        Region contentRootRegion = (Region) loader.load();
        /*
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        origW = d.width;
        origH = d.height;
         */
        //Set a default "standard" or "100%" resolution
        origW = 600;
        origH = 400;
        /*
        //If the Region containing the GUI does not already have a preferred width and height, set it.
        //But, if it does, we can use that setting as the "standard" resolution.
        if ( contentRootRegion.getPrefWidth() == Region.USE_COMPUTED_SIZE )
            contentRootRegion.setPrefWidth( origW );
        else
            origW = contentRootRegion.getPrefWidth();

        if ( contentRootRegion.getPrefHeight() == Region.USE_COMPUTED_SIZE )
            contentRootRegion.setPrefHeight( origH );
        else
            origH = contentRootRegion.getPrefHeight();
         */
        //Wrap the resizable content in a non-resizable container (Group)
        //Group group = new Group( contentRootRegion );
        //Place the Group in a StackPane, which will keep it centered
        StackPane rootPane = new StackPane();
        //rootPane.getChildren().add( group );

        //Create the scene initally at the "100%" size
        //Scene scene = new Scene( rootPane, origW, origH );
        Scene scene = new Scene(contentRootRegion);
        //Bind the scene's width and height to the scaling parameters on the group
        //group.scaleXProperty().bind( scene.widthProperty().divide( origW ) );
        //group.scaleYProperty().bind( scene.heightProperty().divide( origH ) );
        //Set the scene to the window (stage) and show it

        //stage.getIcons().add(new Image("/image/icon.png"));
        //stage.getIcons().add(new Image("http://www.maimt.com/images/cafe.png"));
        stage.setTitle("University Management System");

        //Screen screen = Screen.getPrimary();
        //Rectangle2D bounds = screen.getVisualBounds();
        //stage.setX(bounds.getMinX());
        //stage.setY(bounds.getMinY());
        //stage.setWidth(bounds.getWidth());
        //stage.setHeight(bounds.getHeight());
        //stage.setMaximized(true);
        //stage.setFullScreen(true);
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest((WindowEvent we) -> {
            HibernateUtil.getSessionFactory().close();
        });
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public static void ChangeScene(String FXML) {
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource(FXML));
            Region contentRootRegion = (Region) loader.load();

            /*
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            origW = d.width;
            origH = d.height;

            if ( contentRootRegion.getPrefWidth() == Region.USE_COMPUTED_SIZE )
                contentRootRegion.setPrefWidth( origW );
            else
                origW = contentRootRegion.getPrefWidth();

            if ( contentRootRegion.getPrefHeight() == Region.USE_COMPUTED_SIZE )
                contentRootRegion.setPrefHeight( origH );
            else
                origH = contentRootRegion.getPrefHeight();
             */
            //Wrap the resizable content in a non-resizable container (Group)
            //Group group = new Group( contentRootRegion );
            //Place the Group in a StackPane, which will keep it centered
            //StackPane rootPane = new StackPane();
            //rootPane.getChildren().add( group );
            //Create the scene initally at the "100%" size
            //Scene scene = new Scene( rootPane, origW, origH );
            Scene scene = new Scene(contentRootRegion);
            //Bind the scene's width and height to the scaling parameters on the group
            //group.scaleXProperty().bind( scene.widthProperty().divide( origW ) );
            //group.scaleYProperty().bind( scene.heightProperty().divide( origH ) );
            //Set the scene to the window (stage) and show it
/*
            Screen screen = Screen.getPrimary();
            Rectangle2D bounds = screen.getVisualBounds();

            MainApp.getStage().setX(bounds.getMinX());
            MainApp.getStage().setY(bounds.getMinY());
            MainApp.getStage().setWidth(bounds.getWidth());
            MainApp.getStage().setHeight(bounds.getHeight());
            //MainApp.getStage().setMaximized(true);
             */
            MainApp.getStage().setScene(scene);
            MainApp.getStage().show();

            MainApp.getStage().setOnCloseRequest((WindowEvent we) -> {
                HibernateUtil.getSessionFactory().close();
            });
        } catch (IOException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
            //=======Alert Box Started========//
            Alert alert = new Alert(Alert.AlertType.ERROR);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            //stage.getIcons().add(new Image(stage.getClass().getResource("/image/wrong.png").toString()));
            alert.setTitle("Internal Error");
            alert.setHeaderText("Internal Error Occcured");
            alert.setContentText("Can not load Scene . Please contact software administration ");
            alert.showAndWait();
            //=======Alert Box End========//
        }
    }
    
    /*
    private boolean deleteById(Class<?> type, Serializable id, Session session) {
        Object persistentInstance = session.load(type, id);
        if (persistentInstance != null) {
            session.delete(persistentInstance);
            return true;
        }
        return false;
    }
*/

}
